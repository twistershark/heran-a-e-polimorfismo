#include "circulo.hpp"
#include <string>
#include <iostream>



Circulo::Circulo(){
	set_tipo("Circulo");
	set_base(2.5f);
}


Circulo::Circulo(float raio){
	set_tipo("Circulo");
	set_base(raio);
}


Circulo::Circulo(std::string tipo, float raio){
	set_tipo(tipo);
	set_base(raio);
}

Circulo::~Circulo(){
	std::cout << "Foi destruido o objeto do tipo: " << get_tipo() << std::endl;
}



float Circulo::calcula_area(){
	return (get_base() * get_base()) * 3.14159265358979323846;
}

float Circulo::calcula_perimetro(){
	return (2 * 3.14159265358979323846) * get_base();
}