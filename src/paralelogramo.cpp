#include "paralelogramo.hpp"
#include <string>
#include <iostream>

Paralelogramo::Paralelogramo(){
	set_tipo("Paralelogramo");
	set_base(2.0f);
	set_altura(3.0f);
}


Paralelogramo::Paralelogramo(float base, float altura){
	set_tipo("Paralelogramo");
	set_base(base);
	set_altura(altura);
}


Paralelogramo::Paralelogramo(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}


Paralelogramo::~Paralelogramo(){
	std::cout << "Foi destruido o objeto do tipo: " << get_tipo() << std::endl;
}

float Paralelogramo::calcula_area(){
	return get_base() * get_altura();
}

float Paralelogramo::calcula_perimetro(){
	return (get_base() + get_altura()) * 2;
}