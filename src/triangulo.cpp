#include "triangulo.hpp"
#include <string>
#include <iostream>



Triangulo::Triangulo(){
	set_tipo("Triângulo");
	set_base(6.0f);
	set_altura(6.0f);
}


Triangulo::Triangulo(float base, float altura){
	set_tipo("Triângulo");
	set_base(base);
	set_altura(altura);
}


Triangulo::Triangulo(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}


Triangulo::~Triangulo(){
	std::cout << "Foi destruido o objeto do tipo: " << get_tipo() << std::endl;
}

float Triangulo::calcula_area(){
	return (get_base() * get_altura()) / 2;
}
float Triangulo::calcula_perimetro(){
	return 3 * get_base();
}
