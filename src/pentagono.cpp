#include "pentagono.hpp"
#include <string>
#include <iostream>


Pentagono::Pentagono(){
	set_tipo("Pentágono");
	set_base(3.0f);
	set_altura(3.0f);
}


Pentagono::Pentagono(float base, float altura){
	set_tipo("Pentágono");
	set_base(base);
	set_altura(altura);
}


Pentagono::Pentagono(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}


Pentagono::~Pentagono(){
	std::cout << "Foi destruido o objeto do tipo: " << get_tipo() << std::endl;
}

float Pentagono::calcula_area(){
	return ((get_base() * get_altura()) / 2) * 5;
}

float Pentagono::calcula_perimetro(){
	return get_base() * 5;
}