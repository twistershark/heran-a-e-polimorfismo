#include "quadrado.hpp"
#include <string>
#include <iostream>



Quadrado::Quadrado(){
	set_tipo("Quadrado");
	set_base(3.0f);
	set_altura(get_base());
}


Quadrado::Quadrado(float base, float altura){
	set_tipo("Quadrado");
	set_base(base);
	set_altura(altura);	
	if (get_base() != get_altura())
		throw(1);
}


Quadrado::Quadrado(std::string tipo, float base){
	set_tipo(tipo);
	set_base(base);
	set_altura(get_base());	
	if (get_base() != get_altura())
		throw(1);
}


Quadrado::~Quadrado(){
	std::cout << "Foi destruido o objeto do tipo: " << get_tipo() << std::endl;
}

float Quadrado::calcula_area(){
	return get_base() * get_altura();
}
float Quadrado::calcula_perimetro(){
	return 2*get_base() + 2 * get_altura(); 
}