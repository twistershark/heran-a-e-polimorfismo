#include "hexagono.hpp"
#include <iostream>
#include <string>


Hexagono::Hexagono(){
	set_tipo("Hexágono");
	set_base(2.0f);
	set_altura(2.0f);
}

Hexagono::Hexagono(float base, float altura){
	set_tipo("Hexágono");
	set_base(base);
	set_altura(altura);
}

Hexagono::Hexagono(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Hexagono::~Hexagono(){
	std::cout << "Foi destruido o objeto do tipo: " << get_tipo() << std::endl;
}

float Hexagono::calcula_area(){
	return (get_altura() * get_base() / 2) * 6;
}

float Hexagono::calcula_perimetro(){
	return get_base() * 6;
}