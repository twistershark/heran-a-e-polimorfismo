#include <iostream>
#include <string>
#include <vector>

#include "quadrado.hpp"
#include "triangulo.hpp"
#include "pentagono.hpp"
#include "paralelogramo.hpp"
#include "hexagono.hpp"
#include "circulo.hpp"
#include "formageometrica.hpp"

using namespace std;

int main(){

	vector<FormaGeometrica *> formas;

	formas.push_back(new Quadrado());
	formas.push_back(new Triangulo());
	formas.push_back(new Circulo());
	formas.push_back(new Pentagono());
	formas.push_back(new Paralelogramo());
	formas.push_back(new Hexagono());

	for (FormaGeometrica * forma: formas){
		cout << "Tipo: " << forma->get_tipo() << endl;
		cout << "Área: " << forma->calcula_area() << endl;
		cout << "Perímetro: " << forma->calcula_perimetro() << endl;
	}



	return 0;
}