#include "formageometrica.hpp"
#include <string>


class Circulo : public FormaGeometrica{

public:
	Circulo();
	Circulo(float raio);
	Circulo(std::string tipo, float raio);
	~Circulo();
	float calcula_area();
	float calcula_perimetro();
};
