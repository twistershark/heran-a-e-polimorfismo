#include "formageometrica.hpp"
#include <string>

using namespace std;

class Triangulo : public FormaGeometrica{

public:
	Triangulo();
	Triangulo(float base, float altura);
	Triangulo(string tipo, float base, float altura);
	~Triangulo();
	float calcula_area();
	float calcula_perimetro();
};