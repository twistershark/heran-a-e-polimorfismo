#include "formageometrica.hpp"
#include <string>

using namespace std;

class Quadrado : public FormaGeometrica{

public:
	Quadrado();
	Quadrado(float base, float altura);
	Quadrado(string tipo, float base);
	~Quadrado();
	float calcula_area();
	float calcula_perimetro();
};