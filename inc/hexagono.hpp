#include "formageometrica.hpp"
#include <string>

class Hexagono : public FormaGeometrica{

public:
	Hexagono();
	Hexagono(float base, float altura);
	Hexagono(std::string tipo, float base, float altura);
	~Hexagono();
	float calcula_area();
	float calcula_perimetro();
};