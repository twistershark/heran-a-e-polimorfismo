#include "formageometrica.hpp"
#include <string>

class Pentagono : public FormaGeometrica{

public:
	Pentagono();
	Pentagono(float base, float altura);
	Pentagono(std::string tipo, float base, float altura);
	~Pentagono();
	float calcula_area();
	float calcula_perimetro();
};

