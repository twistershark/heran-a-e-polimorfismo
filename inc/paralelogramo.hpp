#include "formageometrica.hpp"
#include <string>

class Paralelogramo : public FormaGeometrica{

public:
	Paralelogramo();
	Paralelogramo(float base, float altura);
	Paralelogramo(std::string tipo, float base, float altura);
	~Paralelogramo();
	float calcula_area();
	float calcula_perimetro();
};